####General Commands

#####Debug single mocha test
`NODE_ENV=testing mocha -g 'unsuccessful login' --exit --inspect-brk=9229 -r ts-node/register ./tests/api-v1/apiLogin.test.ts`
#####Run single mocha test
`NODE_ENV=testing mocha -g 'unsuccessful login' --exit -r ts-node/register ./tests/api-v1/apiLogin.test.ts`
