#!/usr/bin/env bash

setupMysql () {
    sudo service docker start
    docker run -p 33060:3306 --name="fudave" -e MYSQL_ROOT_PASSWORD=root -d mysql:5
    sleep 1s
    docker exec -it builder mysql -uroot -p
}

setupMysql;