#### MySql Docker Commands

##### Start Docker
`sudo service docker start`

##### Start mysql docker command:
`docker run -p 33060:3306 --name="fudave" -e MYSQL_ROOT_PASSWORD=root -d mysql:5`

##### Start mysql docker interactive cli:
`docker exec -it fudave mysql -uroot -p`

##### Create mysql Database:
`CREATE DATABASE fudave CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`
`SET GLOBAL time_zone = 'UTC'`;