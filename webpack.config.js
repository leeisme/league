const path = require('path');
const nodeExternals = require('webpack-node-externals');
const WebpackShellPlugin = require('webpack-shell-plugin');

const {
    NODE_ENV = process.env.NODE_ENV || 'development',
} = process.env;

module.exports = {
    entry: './src/index.ts',
    mode: process.env.NODE_ENV,
    target: 'node',
    devtool: 'eval-source-map',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'index.ts',
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    'ts-loader',
                ]
            }
        ]
    },
    externals: [nodeExternals()],
    plugins: [
        new WebpackShellPlugin({
            onBuildEnd: ['npm run run:dev']
        })
    ],
    watch: NODE_ENV === 'development'
};