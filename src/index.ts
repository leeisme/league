import * as express from 'express';
import {json, urlencoded} from 'body-parser';
import * as logger from 'morgan';
import * as cors from 'cors';
import {config} from './config/config';
import {RouteRegistry} from './routes/RouteRegistry';
import GlobalMiddlewareRegistry from './middleware/GlobalMiddlewareRegistry';
import * as dotenv from 'dotenv';
import * as env from 'env-var';
import * as moment from 'moment-timezone';
import EventRegistry from './events/EventRegistry';

// Set the timezone using moment().
moment.tz.setDefault('UTC');

// Push config value into ENVIRONMENT vars.
dotenv.config();

// Create app and register basic middleware
const app = express();
app.use(json(), urlencoded({extended: true}));
app.use(cors());

if (env.get('NODE_ENV').asString() === 'production') {
    app.use(logger('combined'));
}

// Register global middleware first
new GlobalMiddlewareRegistry(app).register();

// Register application routes
new RouteRegistry(app).register();

// Register Event Handlers
new EventRegistry().register();

// Start the application
app.listen(config.port, () => {
    console.log('server started at http://localhost:' + config.port);
});

module.exports = app;

