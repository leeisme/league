import * as Joi from 'joi';
import { NextFunction, Request, Response } from 'express';

class IdPropertyValidator {

    public check(req: Request, res: Response, next: NextFunction) {

        const schema = {
            id: Joi.number().min(1).required(),
        };

        let values: object;

        if (req.method === 'POST' || req.method === 'PUT') {
            values = req.body;
        } else if (req.method === 'DELETE' || req.method === 'GET') {
            values = req.params;
        }

        const {error, value} = Joi.validate(values, schema);

        if (error) {
            res.status(400).send({
                error: error.details[0].message,
            });
        } else {
            next();
        }
    }
}

export default new IdPropertyValidator();
