import * as Joi from 'joi';
import {NextFunction, Request, Response} from 'express';

class LoginValidator {
    /**
     * Kicks off the process of registering a user.
     * @param req Request
     * @param res Response
     * @param next NextFunction
     */
    public create = (req: Request, res: Response, next: NextFunction) => {
        const schema = {
            firstName: Joi.string().min(2).max(128).required(),
            lastName: Joi.string().min(2).max(128).required(),
            email: Joi.string().email().required(),
            screenname: Joi.string().regex(/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/),
        };

        const {error, value} = Joi.validate(req.body, schema);

        if (error) {
            res.status(400).send({
                error: error.details[0].message,
            });
        } else {
            next();
        }
    }

    public update(
        req: Request,
        res: Response,
        next: NextFunction,
    ) {
        const schema = {
            id: Joi.number().min(1).required(),
            firstName: Joi.string().min(2).max(128).optional(),
            lastName: Joi.string().min(2).max(128).optional(),
            email: Joi.string().email().optional(),
            screenname: Joi.string().regex(/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/).optional(),
        };

        const {error, value} = Joi.validate(req.body, schema);

        if (error) {
            switch (error.details[0].context.key) {
                case 'email': {
                    res.status(400).send({
                        error: 'Valid email address is required',
                    });

                    break;
                }
                case 'screenName': {
                    res.status(400).send({
                        error: 'Valid screenname required. Alphanumeric characters, underscores, hyphens.',
                    });

                    break;
                }
                default: {

                    res.status(400).send({
                        error: error.details[0].message,
                    });

                    break;
                }
            }
        } else {
            next();
        }
    }

    public paged = (req: Request, res: Response, next: NextFunction) => {
        const schema = {
            perPage: Joi.number().integer().min(10).max(100).required(),
            pageNo: Joi.number().integer().min(1).required(),
        };

        const {error, value} = Joi.validate(req.body, schema);

        if (error) {
            res.status(400).send({
                error: error.details[0].message,
            });
        } else {
            next();
        }
    }
}

export default new LoginValidator();
