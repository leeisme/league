import * as Joi from 'joi';
import {NextFunction, Request, Response} from 'express';

class LoginValidator {

    private passRegex: string;

    constructor() {
        if (process.env.NODE_ENV === 'testing') {
            this.passRegex = '(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$';
        } else {
            this.passRegex = '^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$';
        }
    }

    /**
     * Kicks off the process of registering a user.
     * @param req Request
     * @param res Response
     * @param next NextFunction
     */
    public credentials = (req: Request, res: Response, next: NextFunction) => {

        const schema = {
            password: Joi.string().regex(new RegExp(this.passRegex)).required(),
            email: Joi.string().email().required(),
        };

        const {error, value} = Joi.validate(req.body, schema);

        if (error) {
            switch (error.details[0].context.key) {
                case 'email': {
                    res.status(400).send({
                        error: 'You must supply a valid email address',
                    });

                    break;
                }
                case 'password': {
                    res.status(400).send({
                        error: 'You must supply a valid password ' +
                            'The password must contain at least 1 lowercase alphabetical character - ' +
                            'The password must contain at least 1 uppercase alphabetical character - ' +
                            'The password must contain at least 1 numeric character - ' +
                            'The password must be eight characters or longer',
                    });

                    break;
                }
                default: {
                    res.status(400).send({
                        error: error.details[0].message,
                    });

                    break;
                }
            }
        } else {
            next();
        }
    }

    public email(
        req: Request,
        res: Response,
        next: NextFunction,
    ) {
        const schema = {
            email: Joi.string().email().required().min(8),
            screenName: Joi.string().token().required().min(8),
        };

        const {error, value} = Joi.validate(req.body, schema);

        if (error) {
            switch (error.details[0].context.key) {
                case 'email': {
                    res.status(400).send({
                        error: error.details[0].message,
                    });

                    break;
                }
                case 'screenName': {
                    res.status(400).send({
                        error: error.details[0].message,
                    });

                    break;
                }
                default: {

                    res.status(400).send({
                        error: error.details[0].message,
                    });

                    break;
                }
            }
        } else {
            next();
        }
    }

    public token(
        req: Request,
        res: Response,
        next: NextFunction,
    ) {
        const schema = {
            token: Joi.string().min(12).max(112).required(),
        };

        const {error, value} = Joi.validate(req.params, schema);

        if (error) {

            return res.status(400).send({
                message: error.details[0].message,
            });

        } else {
            next();
        }
    }
}

export default new LoginValidator();
