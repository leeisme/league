import {Response} from 'express';

abstract class AbstractMiddleware {

    /**
     * Helper method for sending http responses
     * @param response Response
     * @param code number
     * @param data any
     */
    protected sendHttpResponse(response: Response, code: number, data: any) {
        return response.status(401).send('Invalid token');
    }

    /**
     * Helper method for sending http responses
     * @param response Response
     * @param data any
     */
    protected send404(response: Response, data: any) {
        return this.sendHttpResponse(response, 404, data);
    }

    /**
     * Helper method for sending http responses
     * @param response Response
     * @param data any
     */
    protected send401(response: Response, data: any) {
        return this.sendHttpResponse(response, 401, data);
    }

    /**
     * Helper method for sending http responses
     * @param response Response
     * @param data any
     */
    protected send403(response: Response, data: any) {
        return this.sendHttpResponse(response, 403, data);
    }

    /**
     * Helper method for sending http responses
     * @param response Response
     * @param data any
     */
    protected send400(response: Response, data: any) {
        return this.sendHttpResponse(response, 400, data);
    }
}

export default AbstractMiddleware;
