import {NextFunction, Request, Response} from 'express';
import AbstractMiddleware from './AbstractMiddleware';
import HttpResponseFactory from '../services/HttpResponseFactory';
import JwtService from '../services/JwtService';

class ApiV1AuthMiddleware extends AbstractMiddleware {

    public async validateApiAccess(req: Request, res: Response, next: NextFunction) {

        const token =
            req.body.token ||
            req.params.token ||
            req.query.token ||
            req.headers['x-access-token'];

        if (!token) {
            return HttpResponseFactory.asString(res, 'Invalid token', 401);
        }

        try {
            res.locals.decoded = await JwtService.verify(token);
        } catch (err) {
            let msg = null;

            if (err.expiredAt) {
                msg = 'Invalid token: expired';
            } else {
                msg = 'Invalid token';
            }

            return res.status(401).send({
                error: msg,
            });
        }

        next();
    }
}

export default new ApiV1AuthMiddleware();
