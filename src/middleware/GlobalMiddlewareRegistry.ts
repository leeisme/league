import * as express from 'express';
import {Application} from 'express';
import ApiV1AuthMiddleware from './ApiV1AuthMiddleware';

class GlobalMiddlewareRegistry {

    private app: Application;

    constructor(app: Application) {
        this.app = app;
    }

    public register() {

        const router = express.Router();

        // Guard api calls, must have token, etc.
        router.all('/api/v1/*', ApiV1AuthMiddleware.validateApiAccess);

        this.app.use('', router);
    }
}

export default GlobalMiddlewareRegistry;
