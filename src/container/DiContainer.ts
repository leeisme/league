import 'reflect-metadata';
import {Container} from 'inversify';

import ServiceIdents from '../config/SVC_IDENTS';
// import UserRepository from '../db/repositories/UserRepository';
const container = new Container();

// -------------------------
// Bind Repositories
// -------------------------

// container.bind<PermissionRepository>(ServiceIdents.PERMISSION_REPO)
//     .to(KnexPermissionRepository);

export default container;
