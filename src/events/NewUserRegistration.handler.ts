import EVENT_IDENTS from '../config/EVENT_IDENTS';
import diContainer from '../container/DiContainer';
import AppLogger from '../logger/AppLogger';
import SVC_IDENTS from '../config/SVC_IDENTS';
import PubSub from '../lib/publisher/PubSub';

export default function register() {
    PubSub.addListener(EVENT_IDENTS.USER_REGISTRATION, (data) => {
        diContainer.get<AppLogger>(SVC_IDENTS.LOGGER)
            .debug(`NewUserRegistration handler fired`);
    });
}
