import * as glob from 'glob';
import * as path from 'path';

class EventRegistry {

    public register() {
        glob.sync('./**/*.handler.ts').forEach((file) => {
            if (file.indexOf('EventRegistry.ts') < 0) {
                const resource = path.basename(file, '.ts');
                require(`${__dirname}/${resource}`);
            }
        });
    }
}

export default EventRegistry;

