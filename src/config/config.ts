import * as env from 'env-var';

const config = {
    auth: {
        JW_API_EXPIRY: env.get('JWT_API_TIMEOUT', '5 minutes').asString(),
        JW_SECRET: env.get('JWT_SECRET', null).asString(),
    },
    db: {
        database: env.get('DB_NAME', 'fudave').asString(),
        options: {
            dialect: env.get('DB_DIALECT', 'mysql').asString(),
            host: env.get('DB_HOST', 'tourney.test.test.ts').asString(),
            port: env.get('DB_PORT', '3306').asInt(),
        },
        password: env.get('PASS', 'root').asString(),
        user: env.get('DB_USER', 'root').asString(),
    },
    port: env.get('SERVER_PORT', '3000').asInt(),
};

export { config };
