export default {
    USER_REGISTRATION_CONFIRM: 'user.registration.confirm',
    USER_REGISTRATION: 'users.registration',
};
