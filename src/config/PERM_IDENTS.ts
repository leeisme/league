export default {
    TYPE_NONE: 0,
    TYPE_READ: 1,
    TYPE_WRITE: 2,
    USER_ADMIN_ACCESS: 'admin_general',
    USER_API_ACCESS: 'user_api_access',
    USER_CUSTOMER_ACCESS: 'customer_general',
    USER_DEVELOPER_ACCESS: 'developer_general',
    USER_GENERAL_ACCESS: 'user_general',
};
