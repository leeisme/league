export default {
    ACTION_SCOPE_PRIVATE: 'private',
    ACTION_SCOPE_PUBLIC: 'public',
    TOKEN_TYPE_API: 'api',
    TOKEN_TYPE_REGISTER: 'register',
    USER_STATUS_REGISTERED: 'registered',
    USER_STATUS_REGISTER_REQUEST: 'reg_request',
};
