interface RolePermission {
    id: number;
    perm: number;
    permId: number;
    roleId: number;
}

export default RolePermission;
