interface AuthStrategy {
    /**
     * Uses a simple strategy to determine how narrow a permission check can be.
     * If @roleName is omitted, the search is presumed to mean ANY Role the user
     * belongs to which has the Permission with a grant >= @minPerm.
     *
     * @param userId number
     * @param permName string
     * @param minPerm number Value to test against for authorization.
     * @param roleName string Optional. Use narrow selection available permissions. If
     * omitted, ANY permission belonging to ANY role assigned to
     */
    authed(userId: number, permName: string, minPerm: number, roleName?: string): Promise<boolean>;

    /**
     * Hmmm.
     *
     * @param userId
     * @param permName
     * @param minPerm
     * @param roleName
     */
    notAuthed(userId: number, permName: string, minPerm: number, roleName: string): Promise<boolean>;
}

export default AuthStrategy;
