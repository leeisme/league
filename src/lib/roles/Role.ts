interface Role {

    id: number;

    /**
     * Name of role
     * @return string
     */
    name: string;

    /**
     * Integer permission value. 0=none, 1=read, 2=write
     */
    perm: number;
}

export default Role;
