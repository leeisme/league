interface Permission {
    id?: number;
    name?: string;
    category?: string;
    perm?: number;
}

export default Permission;
