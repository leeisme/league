import Permission from './Permission';
import RolePermission from './RolePermission';

interface RoleManager {

    /**
     * Creates a new Role
     * @param name string
     * @return RoleManager
     */
    createRole(name: string): Promise<boolean>;

    /**
     * Delete a Role
     * @param name
     */
    removeRole(name: string): Promise<boolean>;

    /**
     * Copy an existing role into a new one using newName
     * @param id
     * @param newName
     * @param retainRoles
     */
    copyRole(id: number, newName: string): Promise<boolean>;

    /**
     * Return an array of Permission objects related to roleId.
     * @param roleId number
     */
    getPermsForRole(roleId: number): Promise<Permission[]>;

    /**
     * Retrieve a single permission object based on its name and the name of
     * the role it belongs to.
     * @param roleName string
     * @param permName string
     */
    getRolePermByName(roleName: string, permName: string): Promise<RolePermission>;

    /**
     * Associate a Permission with a Role.
     * @param roleName
     * @param permName string
     * @param permValue number 0=none, 1=read, 2=write
     */
    addRolePerm(roleName: string, permName: string, permValue: number): Promise<boolean>;

    /**
     * Update a roles_permissions association.
     * @param roleName string
     * @param permName string
     * @param permValue number
     */
    updateRolePerm(roleName: string, permName: string, permValue: number): Promise<boolean>;

    /**
     * Removes a Permission from a Role.
     * @param roleName string
     * @param permName string
     */
    removeRolePerm(roleName, permName: string): Promise<boolean>;

    /**
     * Associates a user with a Role.
     * @param userId
     * @param roleName
     */
    addUserToRole(userId: number, roleName: string): Promise<boolean>;

}

export default RoleManager;
