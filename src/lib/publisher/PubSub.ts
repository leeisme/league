import * as events from 'events';
import EventEmitter = NodeJS.EventEmitter;

/**
 * Singleton (yes, I know. Go pound sand) class for simple pub/sub
 * notifications.
 */
class PubSub {

    private static ps: PubSub;
    private emitter: EventEmitter;

    constructor() {
        // Singleton
        if (PubSub.ps) {
            return PubSub.ps;
        }

        this.emitter = new events.EventEmitter();
        PubSub.ps = this;
    }

    public notify(eventName: string, params) {
        this.emitter.emit(eventName, params);
    }

    public addListener(eventName: string, listener: any) {
        this.emitter.addListener(eventName, listener);
    }
}

export default new PubSub();
