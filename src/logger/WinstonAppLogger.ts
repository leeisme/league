import {createLogger, format, Logger, transports} from 'winston';
import AppLogger from './AppLogger';
import {injectable} from 'inversify';

const { combine, timestamp, label, prettyPrint } = format;

@injectable()
class WinstonAppLogger implements AppLogger {

    private logger: Logger;

    constructor() {
        this.logger = createLogger({
            level: 'info',
            format: format.combine(
                format.json(),
                format.splat(),
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss',
                }),
            ),
            transports: [
                // - Write to all logs with level `info` and above to `combined.log`
                new transports.File({ filename: 'logs/combined.log'}),
                // - Write all logs error (and above) to Console/terminal
                new transports.Console(),
            ],
        });
    }

    public alert(msg: string) {
        this.logger.alert(msg);
    }

    public crit(msg: string) {
        this.logger.crit(msg);
    }

    public debug(msg: string) {
        this.logger.debug(msg);
    }

    public emerg(msg: string) {
        this.logger.emerg(msg);
    }

    public error(msg: string) {
        this.logger.error(msg);
    }

    public info(msg: string) {
        this.logger.info(msg);
    }

    public log(level: string, msg: string) {
        this.logger.log(level, msg);
    }

    public notice(msg: string) {
        this.logger.notice(msg);
    }

    public warning(msg: string) {
        this.logger.warning(msg);
    }
}

export default WinstonAppLogger;
