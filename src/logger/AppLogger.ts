
interface AppLogger {

    log(level: string, msg: string);

    // Level 0
    emerg(msg: string);

    // Level 1
    alert(msg: string);

    // Level 2
    crit(msg: string);

    // Level 3
    error(msg: string);

    // Level 4
    warning(msg: string);

    // Level 5
    notice(msg: string);

    // Level 6
    info(msg: string);

    // Level 7
    debug(msg: string);

}

export default AppLogger;
