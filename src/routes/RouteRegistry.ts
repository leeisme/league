import { Application } from 'express';
import { ApiV1Router } from './apiv1.router';

export class RouteRegistry {

    private app: Application;

    constructor(app: Application) {
        this.app = app;
    }

    public register() {
        // Login/Logout, registration, etc...

        // todo-leejenkins keep an eye on the api and how it can be broken out
        this.app.use('/api/v1', ApiV1Router);

    }
}

