import {Container} from 'inversify';
import container from '../container/DiContainer';

class AbstractService {

    protected getDiContainer(): Container {
        return container;
    }
}

export default AbstractService;
