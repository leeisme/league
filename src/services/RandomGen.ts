import * as crypto from 'crypto';

export function genRandomString(len: number): string {
    return crypto.randomBytes(len).toString('hex');
}

export function genRandomNumber(len: number): number {

    const resultStr = genRandomString(len);

    return Number.parseInt(resultStr, 16);
}
