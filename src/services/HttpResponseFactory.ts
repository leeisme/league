import {Response} from 'express';

class HttpResponseFactory {

    public asString(res: Response, msg: string, code: number = 200) {
        return res.status(code).send(msg);
    }

    public asObject(res: Response, obj: object, code: number = 200) {
        return res.status(code).json(obj);
    }
}

export default new HttpResponseFactory();
