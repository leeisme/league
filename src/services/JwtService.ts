import * as jwt from 'jsonwebtoken';
import * as env from 'env-var';

class JwtService {

    public sign(obj) {

        return new Promise((resolve, reject) => {

            const jwtSecret = process.env.JWT_SECRET || null;
            const jwtExpire = process.env.JWT_API_TIMEOUT || null;

            return jwt.sign(
                obj, jwtSecret, {expiresIn: jwtExpire},
                (err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
        });
    }

    public verify(token) {

        return new Promise((resolve, reject) => {
            return new jwt.verify(token, env.get('JWT_SECRET', null).asString(),
                (err, res) => {

                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
        });
    }
}

export default new JwtService();
