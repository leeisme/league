import * as bcrypt from 'bcrypt-nodejs';

class BCryptService {

    public genSalt(value: any, workforce?: number): Promise<string> {

        if (!workforce) {
            workforce = 8;
        }

        return new Promise((resolve, reject) => {
            bcrypt.genSalt(workforce, (err, salt) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(salt);
                }
            });
        });
    }

    public async genHash(value, salt): Promise<string> {

        return new Promise((resolve, reject) => {
            bcrypt.hash(value, salt, null, (err, hash) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(hash);
                }
            });
        });
    }

    public async genSaltAndHash(value, workforce?): Promise<string> {

        if (!workforce) {
            workforce = 8;
        }

        const newSalt = await this.genSalt(value, workforce);

        return new Promise((resolve, reject) => {
            bcrypt.hash(value, newSalt, null, (err, hash) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(hash);
                }
            });
        });
    }

    public async compareHash(unhashed, hashed): Promise<boolean> {

        return new Promise((resolve, reject) => {
           bcrypt.compare(unhashed, hashed, (err, isGood) => {
                if (err) {
                    reject(false);
                } else {
                    resolve(isGood);
                }
           });
        });
    }
}

export default new BCryptService();
