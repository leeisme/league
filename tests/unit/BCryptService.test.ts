process.env.NODE_ENV = 'testing';

import BCryptService from '../../src/services/BCryptService';
import chai = require('chai');

chai.should();

describe('BCryptService', () => {
    async function getHash(value: string) {
        const salt = await BCryptService.genSalt(value);

        return await BCryptService.genHash(value, salt);
    }

    beforeEach(async function() {
        this.timeout(10000);
    });

    it('should hash a string and successfully compare original against its hashed value', async () => {
        const hash1 = await getHash('1234');
        const hash2 = await getHash('1234');

        hash1.should.not.eq(hash2);

        const hash1Match = await BCryptService.compareHash('1234', hash1);
        hash1Match.should.eq(true);
    });

    it('should not successfully compare hashed value with a value different from pre-hashed value', async () => {
        const hash1 = await getHash('1234');

        const hash1Mismatch = await BCryptService.compareHash('12345', hash1);
        hash1Mismatch.should.eq(false);

        const hash2Mismatch = await BCryptService.compareHash('123456', hash1);
        hash2Mismatch.should.eq(false);
    });
});
